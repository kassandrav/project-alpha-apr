from django.forms import ModelForm
from projects.models import Project


# create model form
class CreateForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]
