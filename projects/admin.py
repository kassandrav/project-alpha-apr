from django.contrib import admin
from projects.models import Project


# register project
@admin.register(Project)
class Project(admin.ModelAdmin):
    list_display = [
        "name",
        "description",
        "id",
    ]
